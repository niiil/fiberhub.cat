# FIBerHub

![Version](https://img.shields.io/badge/version-1.2-blue.svg)![Version](https://img.shields.io/badge/release%20date-february%202019-green.svg) ![Version](https://img.shields.io/badge/source%20code%20license-MIT-green.svg) *![Version](https://img.shields.io/badge/contributors-5-green.svg)*

## Què és això?

FIBerHub és un repositori on trobareu tot tipus de material que us ajudarà si estudieu Enginyeria Informàtica a la UPC. Hi trobareu des d'apunts oficials de l'assignatura, exàmens o codis de programes fins a solucions de qüestionaris puntuables I altres eines útils (Conversor SISA, etc.)

## Qui som?

Som un petit grup d'estudiants que vol ajudar als altres oferint solucions per facilitar la vida estudiantil.

Si teniu qualsevol dubte o necessiteu ajuda no dubteu en contactar-nos! Podeu trobar-nos a Instagram: **_fiberhub**

## Vols ajudar-nos?

Vols reportar un bug, contribuir amb un codi o millorar la documentació? Excel·lent! Contacta amb nosaltres i et respondrem al més aviat possible.

